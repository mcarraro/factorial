#include "factorial.h"

#include <gtest/gtest.h>

TEST(FactorialTest, SpecialCases) {
  EXPECT_EQ(1, Factorial::calc(0));
  //ASSERT_EQ(1, Factorial<-1>::result);
}

TEST(FactorialTest, CompositeTests) {
  EXPECT_EQ(2, Factorial::calc(0) + Factorial::calc(1));
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
