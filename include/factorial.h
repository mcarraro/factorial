#ifndef FACTORIAL__HPP
#define FACTORIAL__HPP

//template <int N> struct Factorial {
//    static const long result = N * Factorial<N-1>::result;
//};
//template <> struct Factorial<0> {
//    static const long result = 1;
//};

class Factorial {
public:
  static unsigned long calc(unsigned long n);
};

#endif
