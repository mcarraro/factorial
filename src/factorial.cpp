#include "factorial.h"
//template <int N> struct Factorial {
//    static const long result = N * Factorial<N-1>::result;
//};
//template <> struct Factorial<0> {
//    static const long result = 1;
//};

unsigned long Factorial::calc(unsigned long n)
{
  unsigned long f = 1;
  while( n > 0)
  {
    f = f * n;
    n--;
  }
  return f;
}

